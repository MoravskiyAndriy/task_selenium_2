package com.moravskiyandriy.mailmessagesender;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class MailMessageSenderTest {
    private static final String ACCOUNT_ADDRESS ="NickScorpi@gmail.com";
    private static final String ACCOUNT_PASSWORD="sc0rp198";
    private static final String LETTER_THEME="Test Theme";
    private static final String LETTER_TEXT="Test Text";
    private static final List<String> LETTER_ADDRESSEES= Arrays.asList("moravskiyandriy.98@gmail.com");
    @Test
    void isLetterSentTest() {
        MailMessageSender mailMessageSender = new MailMessageSender();
        int beforeSendingLetterQuantity;
        int afterSendingLetterQuantity;
        mailMessageSender.goToGooglePage();
        mailMessageSender.clickGmailButton();
        mailMessageSender.clickEnterButton();
        mailMessageSender.fillLoginFieldPressNext(ACCOUNT_ADDRESS);
        mailMessageSender.fillPasswordFieldPressNext(ACCOUNT_PASSWORD);
        mailMessageSender.clickWriteButton();
        mailMessageSender.fillToSection(LETTER_ADDRESSEES);
        mailMessageSender.fillThemeSection(LETTER_THEME);
        mailMessageSender.fillTextSection(LETTER_TEXT);
        beforeSendingLetterQuantity=mailMessageSender.getLetterQuantity();
        mailMessageSender.sendFormedLetter();
        afterSendingLetterQuantity=mailMessageSender.getLetterQuantity();
        mailMessageSender.quitDriver();
        assertEquals(beforeSendingLetterQuantity+1,afterSendingLetterQuantity);
    }
}
