package com.moravskiyandriy.mailmessagesender;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class MailMessageSender {
    private static final String GOOGLE_PAGE_ADRESS="https://www.google.com";
    private static final String TEXTFIELD_CSS="div.Am.Al.editable.LW-avf.tS-tW";
    static{
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
    }
    private static WebDriver driver=new ChromeDriver();
    static{
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    void goToGooglePage(){
        driver.get(GOOGLE_PAGE_ADRESS);
    }

    void clickGmailButton(){
        WebElement gmailButton = driver.findElement(By.linkText("Gmail"));
        gmailButton.click();
    }

    void clickEnterButton(){
        WebElement enterButton = driver.findElement(By.linkText("Увійти"));
        enterButton.click();
    }

    void fillLoginFieldPressNext(String input){
        List<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        WebElement loginField = driver.findElement(By.cssSelector("input[type='email']"));
        loginField.clear();
        loginField.sendKeys(input);
        loginField.sendKeys(Keys.ENTER);
    }

    void fillPasswordFieldPressNext(String input){
        List<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        WebElement passwordField = driver.findElement(By.cssSelector("input[name='password']"));
        passwordField.clear();
        passwordField.sendKeys(input);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        passwordField.sendKeys(Keys.ENTER);
    }

    void clickWriteButton(){
        WebElement writeButton = driver.findElement(By.cssSelector("div.T-I.J-J5-Ji.T-I-KE.L3"));
        writeButton.click();
    }

    void fillToSection(List<String> addressees){
        WebElement toField = driver.findElement(By.cssSelector("textarea[name='to']"));
        toField.click();
        StringBuilder input= new StringBuilder();
        for(String addressee:addressees){
            input.append(addressee).append(",");
        }
        input = new StringBuilder(input.substring(0, input.length() - 1));
        toField.sendKeys(input.toString());
        toField.sendKeys(Keys.ENTER);
    }

    void fillThemeSection(String theme){
        WebElement themeField = driver.findElement(By.cssSelector("input[name='subjectbox']"));
        themeField.click();
        themeField.sendKeys(theme);
    }

    void fillTextSection(String text){
        WebElement textField = driver.findElement(By.cssSelector(TEXTFIELD_CSS));
        textField.click();
        StringBuilder finalText= new StringBuilder(text);
        for(int i=0;i<50;i++){
            finalText.append(" ").append(text);
        }
        textField.sendKeys(finalText.toString());
    }

    void sendFormedLetter(){
        WebElement textField = driver.findElement(By.cssSelector(TEXTFIELD_CSS));
        String keysPressed =  Keys.chord(Keys.CONTROL, Keys.ENTER);
        textField.sendKeys(keysPressed);
    }

    private void goToSentLetters(){
        WebElement sentLettersButton = driver.findElement(By.cssSelector("div.TO[data-tooltip='Надіслані']"));
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(sentLettersButton));
        sentLettersButton.click();
    }

    int getLetterQuantity(){
        goToSentLetters();
        return driver.findElements(By.cssSelector(".zA.yO")).size();
    }

    void quitDriver(){
        driver.quit();
    }
}
